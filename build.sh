#!/bin/bash

source .env

REGISTRY_1="emyfr"
REGISTRY_2="registry.gitlab.com/franchini.emiliano"

echo "Build immagine PowerDNS v. " $PDNS_VER

docker build --build-arg PDNS_VER=$PDNS_VER -t pdns-auth:$PDNS_VER .

docker tag pdns-auth:$PDNS_VER $REGISTRY_1/pdns-auth:$PDNS_VER
docker push $REGISTRY_1/pdns-auth:$PDNS_VER

docker tag pdns-auth:$PDNS_VER $REGISTRY_1/pdns-auth:latest
docker push $REGISTRY_1/pdns-auth:latest

docker tag pdns-auth:$PDNS_VER $REGISTRY_2/docker-powerdns:$PDNS_VER
docker push $REGISTRY_2/docker-powerdns:$PDNS_VER

docker tag pdns-auth:$PDNS_VER $REGISTRY_2/docker-powerdns:latest
docker push $REGISTRY_2/docker-powerdns:latest
